// 本地
const codeip = 'http://192.168.0.105';
const ip = 'http://192.168.0.142:8083';
// 测试环境
// const ip = 'http://192.168.0.123:8088/pengpeng-admin';
// const codeip = 'http://192.168.0.123:8088/login';
// 线上环境
// const ip = 'http://211.159.151.130:8083/pengpeng';
// const codeip = 'http://211.159.151.130:8083/login';
//客户提供的环境
// const ip = 'http://insgle.huaren58.com/pengpeng';
// const codeip = 'http://insgle.huaren58.com/login';
const httpRequest = (opts, data) => {
	let httpDefaultOpts = {
		url: codeip + opts.url,
		data: data,
		method: opts.method,
		header: opts.method == 'get' ? {
			'X-Requested-With': 'XMLHttpRequest',
			"Accept": "application/json",
			"Content-Type": "application/json; charset=UTF-8"
		} : {
			'X-Requested-With': 'XMLHttpRequest',
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		},
		dataType: 'json',
	}
	let promise = new Promise(function(resolve, reject) {
		uni.request(httpDefaultOpts).then(
			(res) => {
				// resolve(res[1])
				// debugger
				uni.removeStorage({
					key: 'token',
					success: function(ress) {
						//console.log('success');
					}
				});
				resolve(res[1])
				const data = res[1];
				if (data.header != undefined && data.header != null && data.header != "") {
					const header = data.header.authorization || data.header.Authorization;

					uni.setStorage({
						key: 'token',
						data: header,
						success(res) {
							// debugger
							//console.log(res)
						},
						fail(res) {
							// debugger
							//console.log(res)
						},
						complete(res) {
							// debugger
							//console.log(res)
						}
					})
				}
			}
		).catch(
			(response) => {
				reject(response)
			}
		)
	})
	return promise
};

let _currentRoute = ''

//带Token请求
const httpTokenRequest = (opts, data) => {
	// debugger
	let token = uni.getStorageSync("token")
	let httpDefaultOpts = {
		url: ip + opts.url,
		data: data,
		method: opts.method,
		header: opts.method == 'get' ? {
			'Authorization': token,
			'X-Requested-With': 'XMLHttpRequest',
			"Accept": "application/json",
			"Content-Type": "application/json; charset=UTF-8"
		} : {
			'Authorization': token,
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
		},
		// dataType: 'json',
	}
	// debugger
	return new Promise(function(resolve, reject) {
		uni.request(httpDefaultOpts).then(
			(res) => {

				const data = res[1];
				// //console.log(data === undefined ? true : false,'是否进入网络失败')
				let currentRoute = getCurrentPages()[0].route
				if (data === undefined&&_currentRoute!=currentRoute) {
					
					uni.navigateTo({
						url: '/pages/index/network'
					})
					_currentRoute = currentRoute;
					return

				} else if (data.data.code == 201) {
					uni.clearStorage();
					uni.showToast({
						title: '',
						icon: 'none',
						success() {
							uni.reLaunch({
								url: '/pages/login/login'
							})
							return
						}
					});
				}

				const authorization = data.header.authorization || data.header.Authorization;
				if (authorization != undefined && authorization != "") {
					// debugger
					const header = data.header.authorization;
					uni.setStorage({
						key: 'token',
						data: authorization,
						success(res) {
							// //console.log(res)
						}
					})
				}
				resolve(res[1])
			}
		).catch(
			(response) => {
				// //console.log(response)
				reject(response)
			}
		)
	})
};

export default {
	ip,
	codeip,
	httpRequest,
	httpTokenRequest
}
