import Vue from 'vue'
import App from './App'
// import '@vant/touch-emulator';
Vue.config.productionTip = false
App.mpType = 'app'
uni.getLocation({
    type: 'wgs84',
    success: function (res) {
		uni.setStorageSync('longitude', res.longitude);
		uni.setStorageSync('latitude', res.latitude);
    }
});
const app = new Vue({
    ...App
})
app.$mount()
